package com.example.zendesksdkdemo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationManagerCompat;

import android.os.Bundle;
import android.view.View;

import zendesk.chat.Chat;
import zendesk.chat.ChatConfiguration;
import zendesk.chat.ChatEngine;
import zendesk.chat.ChatProvider;
import zendesk.chat.ChatProvidersConfiguration;
import zendesk.chat.ProfileProvider;
import zendesk.chat.VisitorInfo;
import zendesk.messaging.MessagingActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Chat.INSTANCE.init(this, "TyuLNvYsvq2uRn5qw5mmdVraBuAZdI4E", "203014980532948993");

        VisitorInfo visitorInfo = VisitorInfo.builder()
                .withName("Bob")
                .withEmail("bob@example.com")
                .withPhoneNumber("123456") // numeric string
                .build();

        ProfileProvider profileProvider = Chat.INSTANCE.providers().profileProvider();
        ChatProvider chatProvider = Chat.INSTANCE.providers().chatProvider();

        ChatProvidersConfiguration chatProvidersConfiguration = ChatProvidersConfiguration.builder()
                .withVisitorInfo(visitorInfo)
                .withDepartment("Department Name")
                .build();

        profileProvider.setVisitorInfo(visitorInfo, null);
        chatProvider.setDepartment("Department name", null);

        Chat.INSTANCE.setChatProvidersConfiguration(chatProvidersConfiguration);
    }

    public void zendeskChat(View v) {
        MessagingActivity.builder()
                .withEngines(ChatEngine.engine())
                .show(v.getContext());
    }
}